
$('.banner-slick').slick({
    infinite: true,
    arrows:true,
    nextArrow: '<div class="slick-right"><img src="assets/home/right.svg"></div>',
    prevArrow: '<div class="slick-left"><img src="assets/home/left.svg"></div>',
    dots: false,
    autoplay: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    // fade: true,
    speed: 2000,
    // cssEase: 'ease-in-out',
});